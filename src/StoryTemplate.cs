using System.Collections.Generic;

namespace LoonyGame
{
    public class StoryTemplate
    {
        public List<string> prompt;
        public string story;
    }
}