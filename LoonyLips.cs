using Godot;
using System;
using System.IO;
using System.Collections.Generic;
using LoonyGame;
using Newtonsoft.Json;

public class LoonyLips : Node2D {
    RichTextLabel storyTextNode;
    LineEdit lineEditNode;
    RichTextLabel buttonLabel;

    Dictionary<string, string> texts;

    List<string> player_words = new List<string>();
    List<StoryTemplate> storyTemplates;
    StoryTemplate currentTemplate;

    int currentStory;
    private static Random rnd = new Random();

    public override void _Ready() {
        storyTextNode = GetNode<RichTextLabel>("Blackboard/StoryText");
        lineEditNode = GetNode<LineEdit>("Blackboard/TextBox");
        buttonLabel = GetNode<RichTextLabel>("Blackboard/TextureButton/ButtonLabel");
        LoadTemplatesJson();
        LoadTextsJson();
        currentStory = rnd.Next(0, storyTemplates.Count);
        currentTemplate = storyTemplates[currentStory];
        startNewStory();
    }

    private void LoadTemplatesJson() {
        using (StreamReader r = new StreamReader("Assets/stories.json")) {
            string json = r.ReadToEnd();
            storyTemplates = JsonConvert.DeserializeObject<List<StoryTemplate>>(json);
        }
    }

    private void LoadTextsJson() {
        using (StreamReader r = new StreamReader("Assets/other-strings.json")) {
            string json = r.ReadToEnd();
            texts = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
        }
    }

    private void startNewStory() {
        player_words.Clear();
        lineEditNode.SetVisible(true);
        promptPlayer(texts["intro_text"]);
    }

    private void promptPlayer(string introduction = "") {
        storyTextNode.Text = introduction + String.Format(
            texts["prompt"],
            currentTemplate.prompt[player_words.Count]
        );
    }

    private void tellStory() {
        storyTextNode.Text = String.Format(
            currentTemplate.story,
            player_words.ToArray()
        );
        lineEditNode.QueueFree();
        buttonLabel.Text = texts["again"];
    }

    private bool isStoryDone() {
        return currentTemplate.prompt.Count == player_words.Count;
    }

    private void processUserInput(string newText) {
        player_words.Add(newText);
        if (isStoryDone()) {
            tellStory();
        } else {
            promptPlayer();
        }

        lineEditNode.Text = "";
    }

    private void _on_TextBox_text_entered(String newText) {
        if (isStoryDone()) return;
        processUserInput(newText);
    }

    private void _on_TextureButton_pressed() {
        if (isStoryDone()) {
            GetTree().ReloadCurrentScene();
        } else {
            processUserInput(lineEditNode.Text);
        }
    }
}
